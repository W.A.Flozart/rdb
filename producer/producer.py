import time
import logging
from datetime import datetime
from kafka import KafkaProducer
from json import dumps,  loads
import csv
#logging.basicConfig(level=logging.DEBUG)

delay = 0.2
#messagecounter = 1

producer = KafkaProducer(bootstrap_servers='bd-2:9092, bd-3:9092, bd-4:9092,', value_serializer=lambda K:dumps(K).encode('utf-8'), acks='all')

with open('/home/data/nytaxitrips.csv', 'r') as file:
	reader = csv.DictReader(file, delimiter=",")
	for row in reader:
		tpep_pickup_datetime  = row['tpep_pickup_datetime']
		tpep_dropoff_datetime = row['tpep_dropoff_datetime']
		now = datetime.now()
		pickup_datetime_object = datetime.strptime(tpep_pickup_datetime, '%m/%d/%Y %I:%M:%S %p')
		dropoff_datetime_object = datetime.strptime(tpep_dropoff_datetime, '%m/%d/%Y %I:%M:%S %p')
		diff = dropoff_datetime_object - pickup_datetime_object
		new_pickup_time = now.strftime('%m/%d/%Y %I:%M:%S:%f %p')
		new_dropoff_time = now + diff
		new_dropoff_time_str = new_dropoff_time.strftime('%m/%d/%Y %I:%M:%S:%f %p')
		row['tpep_pickup_datetime'] = new_pickup_time
		row['tpep_dropoff_datetime'] = new_dropoff_time_str
		#print(row)
		#print(messagecounter)
		#messagecounter += 1
		producer.send('nytaxis', row)
		time.sleep(delay)